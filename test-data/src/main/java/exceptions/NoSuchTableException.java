package exceptions;

/**
 * Created by pavel on 4/16/19
 */
public class NoSuchTableException extends Exception {
    public NoSuchTableException(String msg) {
        super(msg);
    }

    public NoSuchTableException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
