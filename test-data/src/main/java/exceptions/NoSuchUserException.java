package exceptions;

/**
 * Created by pavel on 4/16/19
 */

public class NoSuchUserException extends Exception {
    public NoSuchUserException(String msg) {
        super(msg);
    }

    public NoSuchUserException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
