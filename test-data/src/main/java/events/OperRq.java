package events;

import java.io.Serializable;

/**
 * Created by pavel on 4/16/10
 *
 */
public class OperRq implements Serializable {
    private int id;
    private String name;

    public OperRq() {
        this.id = -1;
        this.name = "";
    }
    public OperRq(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
