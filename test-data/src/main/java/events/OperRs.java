package events;

import java.io.Serializable;

/**
 * Created by pavel on 4/16/10
 *
 */
public class OperRs implements Serializable {
    private int id;
    private String name;
    private String email;
    private int errStatus;

    public OperRs() {
        this.id = -1;
        this.errStatus = 0;
        this.name = "";
        this.email = "";
    }

    public OperRs(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.errStatus = 0;
    }

    public OperRs(int id, int errStatus) {
        this.id = id;
        this.errStatus = errStatus;
        this.name = "";
        this.email = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getErrStatus() {
        return errStatus;
    }

    public void setErrStatus(int errStatus) {
        this.errStatus = errStatus;
    }
}
