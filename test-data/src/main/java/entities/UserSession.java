package entities;

/**
 * Created by pavel on 4/16/19
 *
 */
public class UserSession {
    private int id;
    private long sessionId;
    private String operName;

    public UserSession(int id, long sessionId, String operName) {
        this.sessionId = sessionId;
        this.operName = operName;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public long getSessionId() {
        return sessionId;
    }

    public String getOperName() {
        return operName;
    }
}
