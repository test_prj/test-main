package constants;

/**
 * Created by pavel on 4/16/19
 */
public class JmsConstants {
    public final static String REQUEST_QUEUE = "RequestQueue";
    public final static String RESPONSE_QUEUE = "ResponseQueue";
    public final static String REQUEST_ERROR = "Error_In_Request";

    public final static int OK_STATUS = 0;
    public final static int JMS_ERROR = 1;
    public final static int TIMEOUT_ERROR = 2;
    public final static int USER_NOT_FOUND_STATUS = 3;

    private JmsConstants() {}
}
