package constants;

/**
 * Created by pavel on 4/16/19
 */
public class OperationConstants {
    public final static String REQUEST_OBTAINED = "USER_REQUEST_IN_PROCESSING";
    public final static String RESPONSE_PROCESSED = "USER_REQUEST_PROCESSED";
    public final static String RESPONSE_TIMEOUT = "PROCESSING_TIMEOUT";
    public final static String RESPONSE_ERROR = "USER_REQUEST_ERROR";

    public final static String GET_USER = "getUserByName";
}
