package resttask.dao;

import org.springframework.stereotype.Repository;
import resttask.entities.Member;
import resttask.entities.MembersGroup;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by pavel on 4/18/19
 */
@Repository
public class MembersGroupRepositoryImpl implements MembersGroupRepository {
    private static final Member[] array = {new Member("Petr", 51),
            new Member("Dima", 22), new Member("Fedor", 33),
            new Member("Ivan", 55), new Member("Ruslan", 19),
            new Member("Egor", 20), new Member("Alex", 52)};

    private List<MembersGroup> groups = Arrays.asList(
            new MembersGroup("group1", Arrays.asList(array[0],array[1],array[2])),
            new MembersGroup("group2", Collections.singletonList(array[3])),
            new MembersGroup("group3", Arrays.asList(array[4],array[5],array[6])));

    @NotNull
    public List<MembersGroup> getAllGroups() {
        return groups;
    }
}
