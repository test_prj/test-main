package resttask.dao;

import resttask.entities.MembersGroup;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by pavel on 4/18/19
 */
public interface MembersGroupRepository {
    @NotNull
    List<MembersGroup> getAllGroups();
}
