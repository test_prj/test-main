package resttask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import resttask.exceptions.ResourceNotFoundException;
import resttask.ext.Members;
import resttask.mapper.MembersMapper;
import resttask.services.MemberFinder;

import java.util.regex.Pattern;

/**
 * Created by pavel on 4/18/19
 */
@RestController
public class MemberFinderController {
    private final static Pattern NUMERIC = Pattern.compile("\\d{1,3}");

    @Autowired
    private MemberFinder memberFinder;

    @RequestMapping("/find_members")
    public Members findMembersByAge(@RequestParam(value="age") String age) {
        if(age == null || age.equals("")) {
            throw new ResourceNotFoundException("No data found for empty age");
        }
        if(!NUMERIC.asPredicate().test(age)) {
            throw new ResourceNotFoundException("Incorrect number format for age: " + age);
        }
        System.out.print(Integer.parseInt(age, 10));
        return MembersMapper.mapToMemberNameArray(memberFinder.findMembers(Integer.parseInt(age, 10)));
    }
}
