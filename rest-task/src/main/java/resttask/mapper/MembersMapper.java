package resttask.mapper;

import resttask.ext.Members;

import java.util.Objects;
import java.util.Set;

/**
 * Created by pavel on 4/18/19
 */

public class MembersMapper {

    private MembersMapper() {
    }

    public static Members mapToMemberNameArray(Set<String> names) {
        String[] result = new String[0];
        if(Objects.nonNull(names)) {
            result = names.toArray(result);
        }
        return new Members(result);
    }
}
