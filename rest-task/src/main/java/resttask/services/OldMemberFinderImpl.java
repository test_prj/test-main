package resttask.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import resttask.dao.MembersGroupRepository;
import resttask.entities.Member;
import resttask.entities.MembersGroup;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by pavel on 4/18/19
 */
@Service
public class OldMemberFinderImpl implements OldMemberFinder, MemberFinder {

    @Autowired
    private MembersGroupRepository repository;

    @Override
    public Set<String> findOldMembers(List<MembersGroup> groups, int age) {
        if(Objects.nonNull(groups)) {
            return groups.stream().map(MembersGroup::getMembers)
                    .filter(Objects::nonNull)
                    .flatMap(List::stream)
                    .filter(Objects::nonNull)
                    .filter(member -> Objects.nonNull(member.getAge()))
                    .filter(member -> member.getAge() > age)
                    .map(Member::getName)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        }
        return new HashSet<>();
    }

    @Override
    public Set<String> findMembers(int age) {
        return findOldMembers(repository.getAllGroups(), age);
    }

}
