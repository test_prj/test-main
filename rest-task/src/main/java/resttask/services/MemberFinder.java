package resttask.services;

import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by pavel on 4/18/19
 */
public interface MemberFinder {
    @NotNull
    Set<String> findMembers(int age);
}
