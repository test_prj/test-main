package resttask.services;

import resttask.entities.MembersGroup;

import java.util.Set;
import java.util.List;

/**
 * Created by pavel on 4/18/19
 */
public interface OldMemberFinder {
    Set<String> findOldMembers(List<MembersGroup> listMembers, int age);
}
