package resttask.entities;

import java.util.List;

/**
 * Created by pavel on 4/18/19
 */
public class MembersGroup {
    private final String groupName;
    private final List<Member> members;

    public MembersGroup(String groupName, List<Member> members) {
        this.groupName = groupName;
        this.members = members;
    }

    public String getGroupName() {
        return groupName;
    }

    public List<Member> getMembers() {
        return members;
    }
}
