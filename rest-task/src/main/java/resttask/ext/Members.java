package resttask.ext;

/**
 * Created by pavel on 4/18/19
 */
public class Members {
    private String[] members;

    public Members(String[] members) {
        this.members = members;
    }

    public String[] getMembers() {
        return members;
    }
}
