package resttask.controllers;

/**
 * Created by pavel on 4/18/19
 */
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MemberFinderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void positiveFindMemberTest() throws Exception {
        this.mockMvc.perform(get("/find_members").param("age", "50"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.members").isArray())
                .andExpect(jsonPath("$.members", hasItems("Alex","Petr","Ivan")));
    }

    @Test
    public void missingParamFindMemberTest() throws Exception {
        this.mockMvc.perform(get("/find_members")).andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void incorrectAgeFindMemberTest() throws Exception {
        this.mockMvc.perform(get("/find_members").param("age", "cc"))
                .andDo(print()).andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.date").exists())
                .andExpect(jsonPath("$.message")
                        .value("Incorrect number format for age: cc"));
    }

    @Test
    public void emptyAgeFindMemberTest() throws Exception {
        this.mockMvc.perform(get("/find_members").param("age", ""))
                .andDo(print()).andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.date").exists())
                .andExpect(jsonPath("$.message")
                        .value("No data found for empty age"));
    }
}
