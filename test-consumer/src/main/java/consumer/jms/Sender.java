package consumer.jms;

import constants.JmsConstants;
import events.OperRq;
import org.springframework.jms.JmsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * Created by pavel on 4/16/19
 */
@Component
public class Sender {
    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public int sendRequest(int id, @NotNull String name) {
        try {
            jmsTemplate.convertAndSend(JmsConstants.REQUEST_QUEUE, new OperRq(id, name));
            log.info("Sending request id: {} name: {}", id, name);
            return JmsConstants.OK_STATUS;

        } catch (JmsException ex) {
            log.error(ex.getMessage());
            return JmsConstants.JMS_ERROR;
        }
    }
}
