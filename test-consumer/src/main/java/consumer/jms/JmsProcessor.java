package consumer.jms;

import constants.JmsConstants;
import consumer.ext.ExtUser;
import consumer.mapper.ExtUserMapper;
import entities.UserSession;
import events.OperRs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by pavel on 4/16/19
 */
@Component
public class JmsProcessor {
    private static final Logger log = LoggerFactory.getLogger(JmsProcessor.class);

    private volatile Map<Integer, CompletableFuture<ExtUser>> futures = new ConcurrentHashMap<>();

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private Sender msgSender;

    @Autowired
    MessageConverter converter;

    public CompletableFuture<ExtUser> processRequest(@NotNull final UserSession sess, @NotNull String name) {
        final CompletableFuture<ExtUser> future = new CompletableFuture<>();
        int status = msgSender.sendRequest(sess.getId(), name);
        if (status != JmsConstants.OK_STATUS) {
            log.error("JMS error for req id: {}", sess.getId());
            future.complete(new ExtUser(JmsConstants.JMS_ERROR));
        } else {
            futures.put(sess.getId(), future);
        }
        return future;
    }

    @JmsListener(destination = JmsConstants.RESPONSE_QUEUE, containerFactory = "myFactory")
    public void receiveMessage(@NotNull final OperRs response) {
        log.info("Received OperRequest with id: {} name: {}", response.getId(), response.getName());
        executorService.submit(() -> {
            CompletableFuture<ExtUser> future = futures.remove(response.getId());
            if(future == null) {
                log.error("Request for id {} is already processed / status = ", response.getId(), response.getErrStatus());
            }
            else {
                final ExtUser usr = ExtUserMapper.mapToExtUser(response);
                future.complete(usr);
            }
        });

    }
}
