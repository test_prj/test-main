package consumer.controllers;

import constants.JmsConstants;
import constants.OperationConstants;
import consumer.ext.ExtUser;
import consumer.jms.JmsProcessor;
import consumer.repositories.SessionRepository;
import entities.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by pavel on 4/12/19
 *
 */
@RestController
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private JmsProcessor jmsProcessor;

    @Autowired
    private Session currentSession;

    @Autowired
    private SessionRepository sessionRepository;

    @RequestMapping("/user")
    public DeferredResult<ResponseEntity<ExtUser>> getUserByName(@RequestParam(value = "name") String name) {
        UserSession sess = currentSession.getSession(OperationConstants.GET_USER);
        final DeferredResult<ResponseEntity<ExtUser>> deferredResult = new DeferredResult<>(5000l);
        deferredResult.onTimeout(() -> deferredResult
                .setErrorResult(ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT)
                        .body("Request timeout occurred.")));
        if (name == null || "".equals(name.trim())) {
            deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.CONFLICT)
                    .body("Empty user name: " + name));
        } else {
            sessionRepository.createSession(sess);
            String status = processAndSetResult(deferredResult, sess, name);
            log.info("current session id: {}", sessionRepository.getSessionById(sess.getId()).getSessionId());
        }
        return deferredResult;
    }

    private String processAndSetResult(DeferredResult<ResponseEntity<ExtUser>> deferredResult,
                                       final UserSession sess, String name) {
        try {
            final ExtUser usr = jmsProcessor.processRequest(sess, name).get(4900l, TimeUnit.MILLISECONDS);
            if (usr.getErrStatus() != JmsConstants.OK_STATUS) {
                deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.CONFLICT)
                        .body("No such user with name: " + usr.getName()));
                return OperationConstants.RESPONSE_ERROR;
            } else {
                deferredResult.setResult(ResponseEntity.ok(usr));
                return OperationConstants.RESPONSE_PROCESSED;
            }
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            deferredResult.setErrorResult(ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT)
                    .body("Request timeout occurred."));
            return OperationConstants.RESPONSE_TIMEOUT;
        }
    }
}
