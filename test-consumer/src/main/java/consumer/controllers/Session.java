package consumer.controllers;

import entities.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * Created by pavel on 4/16/19.
 */
@Component
public class Session {
    @Autowired
    private AtomicInteger idGenerator;

    public UserSession getSession(@NotNull String operation) {
        int id = idGenerator.incrementAndGet();
        return new UserSession(id, id, operation);
    }
}
