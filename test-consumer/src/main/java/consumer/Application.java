package consumer;

import entities.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.List;


@SpringBootApplication
@EnableJms
@EnableAsync
@EnableCaching
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... strings) throws Exception {

        log.info("Creating tables");

        jdbcTemplate.execute("DROP TABLE user_session IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE user_session(" +
                "id integer, user_session_id integer, oper_name VARCHAR(100), status VARCHAR(100))");

        List<UserSession> usess = jdbcTemplate.query(
                "SELECT id, user_session_id, oper_name FROM user_session WHERE id = ?", new Object[]{1},
                (rs, rowNum) -> new UserSession(rs.getInt("id"),
                        rs.getLong("user_session_id"), rs.getString("oper_name")));

        log.info("user_session size:"+usess.size());
    }
}