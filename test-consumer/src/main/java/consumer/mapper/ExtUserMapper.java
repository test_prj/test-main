package consumer.mapper;

import constants.JmsConstants;
import consumer.ext.ExtUser;
import events.OperRs;

import javax.validation.constraints.NotNull;

/**
 * Created by pavel on 4/16/19
 *
 */
public class ExtUserMapper {
    private ExtUserMapper() {
    }

    public static ExtUser mapToExtUser(@NotNull OperRs response) {
        return (response.getErrStatus() != JmsConstants.OK_STATUS) ?
                new ExtUser(response.getErrStatus()) : new ExtUser(response.getName(), response.getEmail());
    }
}
