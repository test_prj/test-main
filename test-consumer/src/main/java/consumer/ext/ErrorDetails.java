package consumer.ext;

import java.util.Date;

/**
 * Created by pavel on 4/16/19
 *
 */
public class ErrorDetails {
    private final Date date;
    private final String message;

    public ErrorDetails(String message) {
        date = new Date();
        this.message = message;
    }

    public Date getDate() {
        return this.date;
    }

    public String getMessage() {
        return this.message;
    }
}
