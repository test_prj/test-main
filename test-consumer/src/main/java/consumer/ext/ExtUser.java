package consumer.ext;

/**
 * Created by pavel on 4/16/19
 *
 */
public class ExtUser {
    private String name;
    private String email;
    private int errStatus;

    public ExtUser(String name, String email) {
        this.email = email;
        this.name = name;
        this.errStatus = 0;
    }

    public ExtUser(int errStatus) {
        this.email = "";
        this.name = "";
        this.errStatus = errStatus;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getErrStatus() {
        return errStatus;
    }

}
