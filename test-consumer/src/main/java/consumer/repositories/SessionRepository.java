package consumer.repositories;

import consumer.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import entities.UserSession;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by pavel on 4/16/19
 */
@Repository
public class SessionRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @CachePut(value = "cache", key="#id")
    public void createSession(@NotNull UserSession sess) {
        try {
            jdbcTemplate.update("INSERT INTO user_session(id, user_session_id, oper_name, status) VALUES (?,?,?,'')",
                    sess.getId(), sess.getSessionId(), sess.getOperName());
        } catch(DataAccessException ex) {
            throw new ResourceNotFoundException(ex.getMessage());
        }
    }

    @CachePut(value = "cache", key="#id")
    public void updateSession(int id, @NotNull String status) {
        try {
            jdbcTemplate.update("UPDATE user_session SET status = ? WHERE id = ? ", new Integer(id), status);
        } catch(DataAccessException ex) {
            throw new ResourceNotFoundException(ex.getMessage());
        }
    }

    @Cacheable(value = "cache", key="#id")
    public UserSession getSessionById(int id) {
        try {
            List<UserSession> sessions = jdbcTemplate.query(
                    "SELECT id, user_session_id, oper_name FROM user_session WHERE id = ?", new Object[]{id},
                    (rs, rowNum) -> new UserSession(id,
                            rs.getLong("user_session_id"), rs.getString("oper_name")));
            if(sessions.size() == 0) {
                throw new ResourceNotFoundException("Sesssion not found in user_session, id: "+id);
            }
            return sessions.get(0);
        } catch(DataAccessException ex) {
            throw new ResourceNotFoundException(ex.getMessage());
        }
    }

}
