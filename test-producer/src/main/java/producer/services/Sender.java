package producer.services;

import constants.JmsConstants;
import entities.User;
import events.OperRq;
import events.OperRs;
import exceptions.NoSuchUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.concurrent.ExecutorService;

/**
 * Created by pavel on 4/16/19
 */
@Component
public class Sender {
    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void asyncSend(@NotNull OperRq request) {
        executorService.submit(() -> this.sendRequest(request));
    }

    private void sendRequest(@NotNull OperRq request) {
        OperRs rs = null;
        try {
            User user = userRepository.getUser(request.getName());
            rs = new OperRs(request.getId(), user.getName(), user.getEmail());

        } catch (NoSuchUserException ex) {
            rs = new OperRs(request.getId(), JmsConstants.USER_NOT_FOUND_STATUS);
            log.error(ex.getMessage());
        } finally {
            if(rs == null) {
                log.error("Unrecoverable error for request: {}, {}", request.getId(), request.getName());
            }
            else {
                log.info("Sending response with id: {} name: {} email: {}", rs.getId(), rs.getName(), rs.getEmail());
                jmsTemplate.convertAndSend(JmsConstants.RESPONSE_QUEUE, rs);
            }
        }
    }
}
