package producer.services;

import events.OperRq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.converter.MessageConverter;
import constants.JmsConstants;

/**
 * Created by pavel on 4/16/10
 *
 */
@Component
public class Receiver {
    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    @Autowired
    MessageConverter converter;

    @Autowired
    Sender msgSender;

    @JmsListener(destination = JmsConstants.REQUEST_QUEUE, containerFactory = "myFactory")
    public void receiveMessage(OperRq rq) {
        log.info("Received OperRq id: {} name: {}", rq.getId(), rq.getName());
        msgSender.asyncSend(rq);
    }
}
