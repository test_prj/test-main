package producer.services;

import entities.User;
import org.springframework.dao.DataAccessException;
import exceptions.NoSuchUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by pavel on 4/16/19
 */
@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public User getUser(@NotNull String name) throws NoSuchUserException {
        try {
            List<User> users = jdbcTemplate.query("SELECT id, name, email FROM user WHERE name = ? ",
                    new Object[]{name},
                    (rs, rowNum) -> new User(rs.getString("id"),
                            rs.getString("name"), rs.getString("email")));
            if (users.size() == 0) {
                throw new NoSuchUserException("no such user: " + name);
            }
            return users.get(0);
        } catch (DataAccessException ex) {
            throw new NoSuchUserException("DataAccessException for user: " + ex.getMessage(), ex);
        }
    }
}
