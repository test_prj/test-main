package producer;

import events.OperRs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jms.annotation.EnableJms;

import java.util.Arrays;
import java.util.List;

import java.util.stream.Collectors;

/**
 * Created by pavel on 4/16/10
 *
 */
@SpringBootApplication
@EnableJms
public class ProducerApp implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(ProducerApp.class);

    public static void main(String args[]) {
        SpringApplication.run(ProducerApp.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... strings) throws Exception {

        log.info("Creating tables");

        jdbcTemplate.execute("DROP TABLE user IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE user(id SERIAL, name VARCHAR(100), email VARCHAR(100))");

        List<Object[]> splitUpNames = Arrays.asList(
                "Anna anya@info.com", "Ivan ivan@info.com", "Petr petr@info.com", "Olya olya@info.com")
                .stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        jdbcTemplate.batchUpdate("INSERT INTO user(name, email) VALUES (?,?)", splitUpNames);

        jdbcTemplate.batchUpdate("COMMIT");

        jdbcTemplate.query(
                "SELECT id, name, email FROM user WHERE name = ?", new Object[] { "Anna" },
                (rs, rowNum) -> new OperRs(0, rs.getString("name"), rs.getString("email"))
        ).forEach(usr -> log.info(usr.toString()));
    }
}
